package digitalhanji;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class PlayingGrid {
	
	private int size;
	private int length;
	private float scale = 1;
	private GridPane pane = new GridPane();
	private ArrayList<Rectangle> rectangles = new ArrayList<>();


	PlayingGrid(int s, int l){
		this.size = s;
		this.length = l;
		
		for(int r = 0; r<size; r++){
			for(int c = 0; c<size; c++){
				Rectangle rectangle = new Rectangle(length * this.scale, length * this.scale);
				rectangle.setFill(Color.WHITE);
				rectangle.setStroke(Color.BLACK);
				rectangle.setStrokeWidth(0.1);
				
				rectangle.setOnMouseClicked(new EventHandler<MouseEvent>(){
					@Override
					public void handle(MouseEvent event){
						if (rectangle.getFill() == Color.WHITE){
							rectangle.setFill(Color.GREY);
						} else if (rectangle.getFill() == Color.GREY){
							rectangle.setFill(Color.BLACK);
						} else {
							rectangle.setFill(Color.WHITE);
						}
					}
				});
				this.rectangles.add(rectangle);
				this.pane.add(rectangle, r, c);
			}
		}
	}
	
	void rescale(double newScale){
		this.scale = (float) newScale;
		
		for (int i = 0; i < this.rectangles.size(); i++){
			this.rectangles.get(i).setHeight(this.length * this.scale);
			this.rectangles.get(i).setWidth(this.length * this.scale);
		}
	}
	
	int getLength(){
		return this.length;
	}
	
	int getHeight(){
		return this.length * this.size + (this.length/2);
	}
	
	GridPane getGridAsPane(){
		// Create a scene and place the pane in the stage
		return this.pane;
	}

}
