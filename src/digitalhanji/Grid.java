package digitalhanji;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Grid extends Application {

	int size = 10;
	int length = 20;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		PlayingGrid playingGrid = new PlayingGrid(size, length);
		
		GridPane screen = new GridPane();
		screen.add(new BorderRectangle(5, playingGrid.getHeight()).getBorder(), 1, 0);
		screen.add(new BorderRectangle(playingGrid.getHeight(), 5).getBorder(), 2, 1);
		screen.add(playingGrid.getGridAsPane(), 2, 0);
		
		float sceneWidth = length*size + (length/2);
		float sceneHeight = length*size + (length/2);
		float sceneWidthHeightRatio = sceneHeight / sceneWidth;
		// Create a scene and place the pane in the stage
		Scene scene = new Scene(screen, sceneWidth, sceneHeight);

		/*scene.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
		        double newScale = scene.getWidth()/sceneWidth;
		        
		        playingGrid.rescale(newScale);
		        
		        //primaryStage.setWidth(scene.getWidth());
		        primaryStage.setHeight(playingGrid.getGridAsPane().getHeight() + 100);
		        
		    }
		});
		
		*/
		
		primaryStage.setTitle("Hanji"); 
		primaryStage.setScene(scene); // Place the scene in the stage
		primaryStage.show(); // Display the stage
		
	}

	
	/**
	 * The main method is only needed for the IDE with limited
	 * JavaFX support. Not needed for running from the command line.
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
}


